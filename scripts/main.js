/*jslint browser: true*/

/*global $, jQuery, alert, window*/

var window_height = $(window).height();
var window_width = $(window).width();

function getCurrentScroll() {

	'use strict';

	return window.pageYOffset;

}

function fullHeight() {

	$('#home, #slider').css({height: window_height});

}

function parallax() {

	$('.bg-img').parallax("50%", 0.2);


	$('#services').parallax("50%", 0.2);

	$('#services').css('background-attachment', 'fixed');


	$('#clients').parallax("50%", 0.3);

	$('#clients').css('background-attachment', 'fixed');


	$('#counter').parallax("50%", 0.4);

	$('.counter-up').css('background-attachment', 'fixed');


	$('#contact').parallax("50%", 0.2);

	$('#contact').css('background-attachment', 'fixed');

}

$(function () {



	/* ##### define useful var's ###### */

	var $window_height = $(window).height(), HomeSlider;

	/* ##### make full height first layout ##### */

	fullHeight();

	/* ##### add navigation and create slider for home layout ######## */

	HomeSlider = (function () {

		var $navArrows = $('#nav-arrows'),

			$nav = $('#nav-dots > span'),

			slitslider = $('#slider').slitslider({

				onBeforeChange: function (slide, pos) {

					$nav.removeClass('nav-dot-current');

					$nav.eq(pos).addClass('nav-dot-current');


				}

			}),


			init = function () {

				initEvents();

			},

			initEvents = function () {



				// add navigation events

				$navArrows.children(':last').on('click', function () {


					slitslider.next();

					return false;


				});


				$navArrows.children(':first').on('click', function () {


					slitslider.previous();

					return false;


				});


				$nav.each(function (i) {


					$(this).on('click', function (event) {


						var $dot = $(this);


						if (!slitslider.isActive()) {


							$nav.removeClass('nav-dot-current');

							$dot.addClass('nav-dot-current');

						}


						slitslider.jump(i + 1);

						return false;


					});


				});


			};


		return {init: init};

	})();

	HomeSlider.init();


	/* #### - Carousel Setup via data-attr ##### */

	$('#screenshots').carousel({

		interval: 3000

	});


	/* ##### - Carousel navigation - #### */

	$(".carousel-nav a").click(function (e) {

		e.preventDefault();

		var index = parseInt($(this).attr('data-to'));

		$('#screenshots').carousel(index);

		var nav = $('.carousel-nav');

		var item = nav.find('a').get(index);

		nav.find('a.active').removeClass('active');

		$(item).addClass('active');

	});


	/* ##### - Carousel slide - ###### */

	$("#screenshots").on('slide.bs.carousel', function (e) {

		var elements = 4; // change to the number of elements in your nav

		var nav = $('.carousel-nav');

		var index = $('#screenshots').find('.item.active').index();

		index = (index == elements - 1) ? 0 : index + 1;

		var item = nav.find('a').get(index);

		nav.find('a.active').removeClass('active');

		$(item).addClass('active');

	});


	/* ##### - Animate About us Layout - ##### */

	$('.block-center').waypoint(function () {

		$('.block-center').addClass('animated fadeInLeft');

	}, {

		offset: '75%'

	});


	$('.skill').waypoint(function () {

		$('.skill').addClass('animated fadeInRight');


		/* ##### - Skill-bar animation - ##### */

		$('.skillbar').each(function () {

			$(this).find('.skillbar-bar').animate({

				width: $(this).attr('data-percent')

			}, 4000);

		});

	}, {

		offset: '75%'

	});


	/* ##### - Animate Contact Us Layout - ##### */

	$('.address').waypoint(function () {

		$('.address').addClass('animated fadeInLeft');

	}, {

		offset: '75%'

	});


	$('.contact').waypoint(function () {

		$('.contact').addClass('animated fadeInRight');

	}, {

		offset: '75%'

	});

	/* initialize shuffle plugin */
	var $grid = $('#allprojects');
	$grid.shuffle({
		itemSelector: '.project-item', // the selector for the items in the grid
		speed: 500,
		easing: 'ease-out'
	});

	function addTextToContactFormMessage(text) {
		var $msg = $('#contactForm textarea[name=msg]');
		var currentMsgValue = $msg.val();
		$msg.val((currentMsgValue ? currentMsgValue + '\n' : '') + text);
	}

	function clearContactFormMessage() {
		$('#contactForm textarea[name=msg]').val("");
	}

	/**
	 * Nivo Lightbox
	 */
	var lightboxOptions = {
		effect: 'fall',                          // The effect to use when showing the lightbox, fade,fadeScale,slideLeft,slideRight,slideUp,slideDown,fall,
		theme: 'default',                           // The lightbox theme to use
		keyboardNav: true,                          // Enable/Disable keyboard navigation (left/right/escape)
		clickOverlayToClose: true,                  // If false clicking the "close" button will be the only way to close the lightbox
		errorMessage: 'The requested content cannot be loaded. Please try again later.' // Error message when content can't be loaded
	};

	function afterShowProjectsLightboxHandler() {
		var $lightbox = $('.nivo-lightbox-open');
			if ($lightbox.length === 0) {
			return;
		}
		/*var $lightbox = $('.nivo-lightbox-content').css({
			width: '70%',
			margin: '0 auto'
		});*/
		var $lightboxTitle = $lightbox.find('.nivo-lightbox-title');
		var price = $('.nivo-lightbox-title').attr("data-price");
		var $lightboxButton = $('<a href="#contact" data-name="'+$lightboxTitle.text()+'" data-price="'+price+'" class="btn btn-one white add-to-order over-lightbox">Добавить к заказу</a>');

		$lightboxTitle.hide().after($lightboxButton);
		
			$lightboxButton.on('click', function(event) {
			event.preventDefault();
			/*$lightboxButton.hide();*/
			});
		
	}

	function initNivoLightbox() {
		$('.our-services a').nivoLightbox(lightboxOptions);

		$('.projects .mask a').nivoLightbox($.extend({}, lightboxOptions, {
			afterShowLightbox: afterShowProjectsLightboxHandler,
			onPrev: afterShowProjectsLightboxHandler,
			onNext: afterShowProjectsLightboxHandler
		}));
	}

	function closeLightbox() {
		$('.nivo-lightbox-close').click();
	}

	initNivoLightbox();

	/**
	 * Close Lightbox On Submit Button click
	 */
	$('body').on('click', '.nivo-lightbox-open .btn.view-more', function() {
		var lightboxContentHeaderText = $('.nivo-lightbox-content').find('h3').text();
		/*addTextToContactFormMessage(lightboxContentHeaderText);*/
		closeLightbox();
	});

	$('.simple-nav label input').bind('click', function () {

		$('.simple-nav label input').attr('checked', false);

		$('.simple-nav label').find('span').removeClass('active');

		$(this).next().addClass('active');

		$(this).attr('checked', true);
		// get group name from clicked item
		var groupName = $(this).attr('data-group');
		// reshuffle grid
		$grid.shuffle('shuffle', groupName);

		initNivoLightbox();

		return false;

	});


	/* ##### - Add FadeIn animation - ##### */

	$('.hi-icon-effect-5a').waypoint(function () {

		$('.hi-icon-effect-5a').addClass('animated fadeInUp');

	}, {

		offset: '80%'

	});


	/* #### - animated Our team layout - ##### */

	$('.myteam').waypoint(function () {

		$('.myteam').addClass('animated fadeInUp');

	}, {

		offset: '75%'

	});


	$('.count').waypoint(function () {

		$('.count').addClass('animated fadeInUp');

	}, {

		offset: '75%'

	});


	/* ##### - Client carousel - ##### */

	$('#client').carousel({

		interval: 50000,

		pause: "hover"

	});


	/* ##### - About us carousel - ##### */

	$('#about_us').carousel({

		interval: 50000,

		pause: "hover"

	});


	/* ##### - testimonials carousel - ##### */

	$('#testimonials').carousel({

		interval: 10000

	});


	/* ##### - jquery waypoints sticky header - ##### */

	var $header = 200;

	$(window).scroll(function () {

		var scroll = getCurrentScroll();

		if (scroll >= $header) {

			$('.header').addClass('stuck');

		}

		else {

			$('.header').removeClass('stuck');

		}

	});


	/* ##### - remove class preload on load - ##### */

	$("body").removeClass("preloader");


	/* ##### - hover effect for projects layout - ##### */

	if (Modernizr.touch) {



		// handle the adding of hover class when clicked

		$(".portfolio .projects-image").click(function (e) {

			e.preventDefault();

			e.stopPropagation();

			if (!$(this).hasClass("hover")) {

				$(this).addClass("hover");

			}

		});


	} else {



		// handle the mouseenter functionality

		$(".portfolio .projects-image").mouseenter(function () {

			$(this).addClass("hover");

		})

			// handle the mouseleave functionality

			.mouseleave(function () {

				$(this).removeClass("hover");

			});

	}


	/* ##### - Smooth Scrolling and Active Class on Navigation - ##### */

	var aChildren = $("nav li").children();

	var aArray = []; // create the empty aArray

	for (var i = 0; i < aChildren.length; i++) {

		var aChild = aChildren[i];

		var ahref = $(aChild).attr('href');

		aArray.push(ahref);

	}


	$(window).scroll(function () {

		var windowPos = $(window).scrollTop(); // get the offset of the window from the top of page

		var windowHeight = $(window).height(); // get the height of the window

		var docHeight = $(document).height();


		for (var i = 0; i < aArray.length; i++) {

			var theID = aArray[i];

			var divPos = $(theID).offset().top; // get the offset of the div from the top of page

			var divHeight = $(theID).height(); // get the height of the div in question

			if (windowPos >= divPos && windowPos < (divPos + divHeight)) {

				$("a[href='" + theID + "']").addClass("active");

			} else {

				$("a[href='" + theID + "']").removeClass("active");

			}

		}


		if (windowPos + windowHeight == docHeight) {

			if (!$("nav li:last-child a").hasClass("active")) {

				var navActiveCurrent = $(".active").attr("href");

				$("a[href='" + navActiveCurrent + "']").removeClass("active");

				$("nav li:last-child a").addClass("active");

			}

		}

	});


	$('a[href*=#]:not([href=#])').click(function () {

		if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {


			var target = $(this.hash);

			target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');

			if (target.length) {

				$('html,body').animate({

					scrollTop: target.offset().top

				}, 1000);


				return false;

			}

		}

	});

	function initReplaceLightboxImages() {
		widthToGetMobImages = 720;
		if (window_width <= widthToGetMobImages) {
			$('.projects-image .gallery').each(function () {
				href = $(this).attr("href");
				splittedHref = href.split('/');
				newHref = splittedHref[3].split('.')[0];
				newHref = splittedHref[0] + '/' + splittedHref[1] + '/' + splittedHref[2] + '/' + newHref + '_m.jpg';
				$(this).attr("href", newHref);
			});
		}
	}

	initReplaceLightboxImages();


	/* ##### - counter up - ##### */

	$('.milestone-count').counterUp({

		delay: 20,

		time: 2000

	});


	/* ##### - hover on socials - ##### */

	$(".social li a").mouseenter(function () {

		$(this).addClass("hover");

	})

		// handle the mouseleave functionality

		.mouseleave(function () {

			$(this).removeClass("hover");

		});
		

/*#### - AJAX promo Contact Form - ####*/
	$('#btn_submit').click(function (event) {

		event.preventDefault();

		var proceed = true;

		//simple validation at client's end

		//loop through each field and we simply change border color to red for invalid fields

		var emptyFieldsCount = 0;
		var currentEmptyField = "";

		$("#promoForm input[required=true], #promoForm textarea[required=true]").each(function () {

			$(this).removeClass('error');

			if (!$.trim($(this).val())) { //if this field is empty

				emptyFieldsCount ++;

				currentEmptyField = $(this).attr("placeholder");

				$(this).addClass('error'); //change border color to red

				$('#promoForm p.alert').html('<i class="fa fa-exclamation-triangle"></i> Пожалуйста, заполните выделенные поля').fadeIn();

				proceed = false; //set do not proceed flag

			}


			//check invalid email

			var email_reg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

			if ($(this).attr("type") == "email" && !email_reg.test($.trim($(this).val()))) {

				$('#promoForm p.alert').html('<i class="fa fa-exclamation-triangle"></i> Пожалуйста, введите корректный email адрес').fadeIn();

				$(this).addClass('error'); //change border color to red

				proceed = false; //set do not proceed flag

			}

		});

		if (proceed) { //everything looks good! proceed...

			//$('p.alert').html('').fadeOut();

			//get input field values data to be sent to server

			post_data = {

				'name': $('input[name=promo_name]').val(),

				'email': $('input[name=promo_email]').val(),

				'phone': $('input[name=promo_phone]').val()

			}

			//Ajax post data to server

			var output = '';

			$.post('scripts/promo.php', post_data, function (response) {

				if (response.type == 'error') {

					output = '<i class="fa fa-exclamation-triangle"></i>' + response.text + '';

					$('#promoForm p.alert').html(output).fadeIn();

				} else {

					output = '<h3>' + response.text + '</h3>';

					$('#promo_results').addClass('open').html(output);

					$('#promoForm').fadeOut();


					$("#promoForm  input:not(input[type=submit]), #promoForm textarea").val('');

				}

			}, 'json');

		} else {
			if(emptyFieldsCount==1) {
				$('#promoForm p.alert').html('<i class="fa fa-exclamation-triangle"></i> Пожалуйста, заполните поле "'+currentEmptyField+'"').fadeIn();
			}
		}

		return false;

	});


/*#### - End AJAX promo Contact Form - ####*/			
		
	/* ##### - Ajax Contact Form - #####*/

	$('#submit').click(function (event) {

		event.preventDefault();

		var proceed = true;

		//simple validation at client's end

		//loop through each field and we simply change border color to red for invalid fields

		var emptyFieldsCount = 0;
		var currentEmptyField = "";

			$("#contactForm input[required=true], #contactForm textarea[required=true]").each(function () {

			$(this).removeClass('error');

			if (!$.trim($(this).val())) { //if this field is empty

				emptyFieldsCount ++;

				currentEmptyField = $(this).attr("placeholder");

				$(this).addClass('error'); //change border color to red

				$('#contactForm p.alert').html('<i class="fa fa-exclamation-triangle"></i> Пожалуйста, заполните выделенные поля').fadeIn();

				proceed = false; //set do not proceed flag

			}


			//check invalid email

			var email_reg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

			if ($(this).attr("type") == "email" && !email_reg.test($.trim($(this).val()))) {

				$('#contactForm p.alert').html('<i class="fa fa-exclamation-triangle"></i> Пожалуйста, введите корректный email адрес').fadeIn();

				$(this).addClass('error'); //change border color to red

				proceed = false; //set do not proceed flag

			}

		});

		if (proceed) { //everything looks good! proceed...

			//$('p.alert').html('').fadeOut();

			//get input field values data to be sent to server

			post_data = {

				'name': $('input[name=name]').val(),

				'email': $('input[name=email]').val(),

				'msg': $('textarea[name=msg]').val(),
				
				'date': $('input[name=date]').val(),
				
				'phone': $('input[name=phone]').val()

			}

			//Ajax post data to server

			var output = '';

			$.post('scripts/contact.php', post_data, function (response) {

				if (response.type == 'error') {

					output = '<i class="fa fa-exclamation-triangle"></i>' + response.text + '';

					$('#contactForm p.alert').html(output).fadeIn();

				} else {

					output = '<h3>' + response.text + '</h3>';

					$('#contact_results').addClass('open').html(output);

					$('#contactForm').fadeOut();


					$("#contactForm  input:not(input[type=submit]), #contactForm textarea").val('');

				}

			}, 'json');

		} else {
			if(emptyFieldsCount==1) {
				$('#contactForm p.alert').html('<i class="fa fa-exclamation-triangle"></i> Пожалуйста, заполните поле "'+currentEmptyField+'"').fadeIn();
			}
		}

		return false;

	});


	//Add Modernizr test for IOS device

	Modernizr.addTest('isiOS', function () {

		return navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false

	});

	//usage

	if (!Modernizr.isiOS) {

		parallax();

	}

	$('.order-btn-js').click(function() {
		//clearContactFormMessage();      //очищение формы перед добавлением текста из калькулятора
		$('.main-calculator').addClass("hided");
		$('.main-calculator').removeClass("shown");
		$('.calc-content').hide();
		var totalSumm = $('.total-summ').html();

		$('.item-row-js').each(function() {
			if($(this).css("text-decoration")!="line-through") {
				addTextToContactFormMessage($(this).text());
			}
		})
		addTextToContactFormMessage('Общая стоимость: '+totalSumm+' руб.');

		resizeArea( 'msg',150,450 );

	})

	///////////////////////////////////CALC
	$(".main-calc-content-js").mCustomScrollbar();

	function addSpaces(nStr){
		nStr += '';
		x = nStr.split('.');
		x1 = x[0];
		x2 = x.length > 1 ? '.' + x[1] : '';
		var rgx = /(\d+)(\d{3})/;
		while (rgx.test(x1)) {
			x1 = x1.replace(rgx, '$1' + ' ' + '$2');
		}
		return x1 + x2;
	}

	var marginTopBottomCalc = 40;//////////////////////Отступы сверху и снизу от калькулятора
	var calcMaxHeight = window_height - 214 - marginTopBottomCalc * 2;

	$('.main-calc-content-js').css("max-height",calcMaxHeight);


	function refreshTotalSumm(totalSumm) {
		$('.total-summ').attr("data-total-summ",totalSumm);
		$('.total-summ').html(addSpaces(totalSumm));
	}

	$(document.body).on('click','.add-to-order',function() {
		if($('.main-calculator').hasClass("no-display")) {
			$('.main-calculator').removeClass("no-display");
			$('.main-calculator').addClass("shown");
		}

		var id = $('.item-row-js').length + 1;
		var price = $(this).attr("data-price");
		if(!$.isNumeric(price)) {
			price = 0;
		}
		var name = $(this).attr("data-name");
		var totalSumm = $('.total-summ').attr("data-total-summ") * 1 + price * 1;
		var itemRow = '<tr><td id="calc-item'+id+'" class="item-row item-row-js" data-price="'+price+'">'+name+'</td><td id="calc-item-del'+id+'" class="del-item-button del-item-button-js"></td></tr>';

		$(itemRow).appendTo($('.calc-content-js'));

		refreshTotalSumm(totalSumm);
	})

	$(document.body).on('click','.item-row-js',function() {
		if($(this).css("text-decoration")!="line-through") {
			$(this).css("text-decoration", "line-through");
			var price = $(this).attr("data-price");
			var totalSumm = $('.total-summ').attr("data-total-summ") * 1 - price * 1;

			refreshTotalSumm(totalSumm);
		} else {
			$(this).css("text-decoration", "none");
			var price = $(this).attr("data-price");
			var totalSumm = $('.total-summ').attr("data-total-summ") * 1 + price * 1;

			refreshTotalSumm(totalSumm);
		}
	})
	$(document.body).on('click','.del-item-button-js',function() {
		var numId = parseInt($(this).attr("id").replace(/\D+/g,""));
		var price = $('#calc-item'+numId).attr("data-price");
		var totalSumm = $('.total-summ').attr("data-total-summ") * 1 - price * 1;
		$('#calc-item'+numId).remove();
		$(this).remove();

		refreshTotalSumm(totalSumm);
	})

	$(document.body).on('click','.arrow-calc-left-js, .add-to-order',function() {
		$('.main-calculator').addClass("shown");
		$('.main-calculator').removeClass("hided");
		$('.calc-content').show();
	})

	$('.arrow-calc-right-js').click(function() {
		$('.main-calculator').addClass("hided");
		$('.main-calculator').removeClass("shown");
		$('.calc-content').hide();
	})
	$('.hide-calc-content-js').click(function() {
		$('.calc-content-js').toggle();
	})

	/////////////////////////////////CALC

	function resizeArea(text_id, minHeight, maxHeight)
	{
		var area = $('#'+text_id);
		var area_hidden = $('#'+text_id+ "_hidden");
		var text = '';
		area.val().split("\n").forEach( function(s) {
			text = text + '<div>' + s.replace(/\s\s/g, ' &nbsp;') + '&nbsp;</div>'+"\n";
		} );
		area_hidden.html(text);
		var height = area_hidden.innerHeight() + 15;
		height = Math.max(minHeight, height);
		height = Math.min(maxHeight, height);
		area.css("height",height);
	}

});

$(window).resize(function(){
	var marginTopBottomCalc = 40;//////////////////////Отступы сверху и снизу от калькулятора

	var calcMaxHeight = $(window).height() - 214 - marginTopBottomCalc * 2;

	$('.main-calc-content-js').css("max-height",calcMaxHeight);
	$(".main-calc-content-js").mCustomScrollbar("update");
});