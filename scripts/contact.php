<?php

   if($_POST) {

        $to_email       = "siteorder@multiview.ru"; //Recipient email, Replace with own email here

         //check if its an ajax request, exit if not

        if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {

        

            $output = json_encode(array( //create JSON data

                'type'=>'error', 

                'text' => 'Sorry Request must be Ajax POST'

            ));

            die($output); //exit script outputting json data

        }

        //Sanitize input data using PHP filter_var().

        $name      = filter_var($_POST["name"], FILTER_SANITIZE_STRING);
		
		$date      = filter_var($_POST["date"], FILTER_SANITIZE_STRING);

        $email     = filter_var($_POST["email"], FILTER_SANITIZE_EMAIL);

        $msg       = filter_var($_POST["msg"], FILTER_SANITIZE_STRING);
		
		$phone     = filter_var($_POST["phone"], FILTER_SANITIZE_STRING);



        //additional php validation

        if(strlen($name)<4 || strlen($name)>100){ // If length is less than 4 it will output JSON error.

            $output = json_encode(array('type'=>'error', 'text' => 'Пожалуйста, заполните поле "Ваше имя и название проекта"'));

            die($output);

        }

        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){ //email validation

            $output = json_encode(array('type'=>'error', 'text' => 'Пожалуйста, введите корректный email адрес'));

            die($output);

        }

        if(strlen($msg)<3 || strlen($msg)>1200){ //check emtpy message

            $output = json_encode(array('type'=>'error', 'text' => 'Пожалуйста, заполните графу "Описание проекта"'));

            die($output);

        }



        //email body

        $message_body = $name."\r\n".$date."\r\n".$msg."\r\n\r\n-".$name."\r\nEmail : ".$email."\r\nPhone : ".$phone; 

        $subject = "Multivew.ru From: ".$name;



        //proceed with PHP email.

        $headers = 'From: ' .$name.'' . "\r\n" .

        'Reply-To: '.$email.'' . "\r\n" .

        'X-Mailer: PHP/' . phpversion() . "\r\n";

        $send_mail = mail($to_email, $subject, $message_body, $headers);



        if(!$send_mail){

            //If mail couldn't be sent output error. Check your PHP email configuration (if it ever happens)

            $output = json_encode(array('type'=>'error', 'text' => 'Could not send mail! Please check your PHP mail configuration.'));

            die($output);

        } else {

            $output = json_encode(array('type'=>'message', 'text' => 'Спасибо за обращение, мы обработаем ваш запрос в ближайшее время.'));

            die($output);

        }

   }



?>